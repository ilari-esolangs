#ifndef _stdlibrary__h__included__
#define _stdlibrary__h__included__

struct object;
struct vat;

/* Standard library. */
struct stdlibrary
{
	struct object* stdin;
	struct object* stdout;
	struct object* stderr;
};

/******************************************************************************
 *
 * DESCRIPTION:
 *	Initialize standard library objects for given vat.
 *
 * PARAMETERS:
 *	vat			The vat to create them in.
 *
 * RETURN VALUE:
 *	Standard library objects.
 *
 *****************************************************************************/
struct stdlibrary* stdlibrary_init(struct vat* vat);

#endif
