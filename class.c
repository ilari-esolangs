#include <stdio.h>
#include "class.h"

/*****************************************************************************/
struct class
{
	/* Class number. */
	size_t c_class_number;
	/* Number of fields. */
	size_t c_fields;
	/* Number of locals. */
	size_t c_locals;
	/* Body text. */
	unsigned char* c_body;
	/* Body text length. */
	size_t c_body_length;
	/* Next entry. */
	struct class* c_next;
};

/*****************************************************************************/
static struct class* c_list_head = NULL;
static struct class* c_list_tail = NULL;
static size_t c_first_unalloc = 0;

/*****************************************************************************/
struct class* class_search(size_t number)
{
	struct class* cur = c_list_head;

	while(cur) {
		if(number == cur->c_class_number)
			return cur;
		cur = cur->c_next;
	}
	/* No match. */
	return NULL;
}

/*****************************************************************************/
void class_register(struct class* class)
{
	struct class* nxt = class->c_next;
	
	class->c_next = NULL;
	class->c_class_number = c_first_unalloc++;
	if(c_list_head)
		c_list_tail = c_list_tail->c_next = class;
	else
		c_list_tail = c_list_head = class;

	if(nxt)
		class_register(nxt);
}

/*****************************************************************************/
void class_unregister_all()
{
	struct class* class;
	while(c_list_head) {
		class = c_list_head;
		c_list_head = c_list_head->c_next;
		free(class->c_body);
		free(class);
	}
}

/*****************************************************************************/
struct class* class_make(size_t fields, size_t locals, 
	unsigned char* instructions, size_t ins_len)
{
	struct class* class;
	class = malloc(sizeof(struct class));
	if(!class) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}

	class->c_fields = fields;
	class->c_locals = locals;
	class->c_body = instructions;
	class->c_body_length = ins_len;
	class->c_next = NULL;
	return class;
}


/*****************************************************************************/
size_t class_field_count(struct class* class)
{
	return class->c_fields;
}

/*****************************************************************************/
size_t class_local_count(struct class* class)
{
	return class->c_locals;
}

/*****************************************************************************/
size_t class_ins_count(struct class* class)
{
	return class->c_body_length;
}

/*****************************************************************************/
unsigned char class_get_ins(struct class* class, size_t position)
{
	if(position >= class->c_body_length)
		return 0;
	return class->c_body[position];
}
