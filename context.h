#ifndef _context__h__included__
#define _context__h__included__

struct vat;
struct message;
struct object;
struct stack;
#include <stdlib.h>
#include "lock.h"

/* Context. */
struct context;

/******************************************************************************
 *
 * DESCRIPTION:
 *	Pop object from stack. Popping empty stack gives NULL.
 *
 * PARAMETERS:
 *	ctx			The context.
 *
 * RETURN VALUE:
 *	The object popped. 
 *
 *****************************************************************************/
struct object* context_pop(struct context* ctx);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Push object to stack.
 *
 * PARAMETERS:
 *	ctx			The context.
 *	obj			The object to push. 
 *
 *****************************************************************************/
void context_push(struct context* ctx, struct object* obj);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Notify context that execution is starting.
 *
 * PARAMETERS:
 *	ctx			The context.
 *
 * RETURN VALUE:
 *	1 if suitable message was found, 0 if there is no suitable message.
 *
 *****************************************************************************/
unsigned context_prepare(struct context* ctx);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Notify context that execution is finished.
 *
 * PARAMETERS:
 *	ctx			The context.
 *
 *****************************************************************************/
void context_unprepare(struct context* ctx);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Calculate number of stack slots.
 *
 * PARAMETERS:
 *	ctx			The context.
 *
 *****************************************************************************/
size_t context_stacksize(struct context* ctx);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Single-step context.
 *
 * PARAMETERS:
 *	ctx			The context.
 *
 *****************************************************************************/
void context_singlestep(struct context* ctx);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Create context.
 *
 * PARAMETERS:
 *	vat			The vat to use.
 *
 * RETURN VALUE:
 *	The context. Pass to free when done.
 *
 *****************************************************************************/
struct context* context_create(struct vat* vat);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Grab context lock.
 *
 * PARAMETERS:
 *	ctx			Context to lock.
 *
 *****************************************************************************/
void context_lock(struct context* ctx);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Ungrab context lock.
 *
 * PARAMETERS:
 *	ctx			Context to unlock.
 *
 *****************************************************************************/
void context_unlock(struct context* ctx);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Queue message into message queue.
 *
 * PARAMETERS: 
 *	context			The context to use.
 *	message			The message to queue.
 *
 *****************************************************************************/
void context_queue_message(struct context* context, struct message* message);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Execute message handler.
 *
 * PARAMETERS: 
 *	context			The context to use.
 *
 * RETURN VALUE:
 *	1 if message got handled, 0 if not.
 *
 *****************************************************************************/
unsigned context_execute_msghandler(struct context* ctx);


#endif
