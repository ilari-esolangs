all: capfuck capfuck.debug 

capfuck: capfuck.o sane-ctype.o class.o object.o message.o lock.o vat.o context.o stdlibrary.o parser.o
	gcc -o $@ $^
	strip $@

capfuck.debug: capfuck.debug.o sane-ctype.debug.o class.debug.o object.debug.o message.debug.o lock.debug.o vat.debug.o context.debug.o stdlibrary.debug.o parser.debug.o
	gcc -o $@ $^

%.o: %.c
	gcc -Wall -O2 -std=c99 -c -o $@ $<

%.debug.o: %.c
	gcc -Wall -g -std=c99 -c -o $@ $<

clean:
	rm capfuck capfuck.debug *.o 

