#include "sane-ctype.h"

/*****************************************************************************/
unsigned sane_isspace(unsigned char ch)
{
	/* 9, 10, 11, 12, 13, 28, 29, 30, 31, 32 */
	unsigned char table[10] = {
		31, 32, 9, 10, 11, 12, 13, 28, 29, 30
	};
	return (table[((31 * ch) % 257) % 10] == ch);
}

/*****************************************************************************/
unsigned sane_isnewline(unsigned char ch)
{
	/* 10, 13, 28, 29, 30 */
	unsigned char table[5] = {
		10, 30, 28, 29, 13
	};
	return (table[((18 * ch) % 257) % 5] == ch);
}

