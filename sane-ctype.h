#ifndef _sane_ctype__h__included__
#define _sane_ctype__h__included__

/******************************************************************************
 * 
 * DESCRIPTION:
 *	Is the character whitespace according to ASCII?
 *
 * PARAMETERS:
 *	ch			The character.
 *
 * RETURN VALUE:
 *	1 if character is whitespace, 0 otherwise.
 *
 *****************************************************************************/
unsigned sane_isspace(unsigned char ch);

/******************************************************************************
 * 
 * DESCRIPTION:
 *	Is the character line change according to ASCII?
 *
 * PARAMETERS:
 *	ch			The character.
 *
 * RETURN VALUE:
 *	1 if character is line change character, 0 otherwise.
 *
 *****************************************************************************/
unsigned sane_isnewline(unsigned char ch);

#endif
