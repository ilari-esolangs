#ifndef _class__h__included__
#define _class__h__included__

#include <stdlib.h>

/* Class table entry. */
struct class;

/******************************************************************************
 *
 * DESCRIPTION:
 *	Search for class with given number.
 *
 * PARAMETERS:
 *	number			Number of class to search.
 *
 * RETURN VALUE:
 *	Class structure, or NULL if no such class exists.
 *
 *****************************************************************************/
struct class* class_search(size_t number);

/******************************************************************************
 *
 * DESCRIPTION: 
 *	Register new class(es).
 *
 * PARAMETERS:
 *	class			The class to register.
 *
 *****************************************************************************/
void class_register(struct class* class);

/******************************************************************************
 *
 * DESCRIPTION: 
 *	Unregister all classes.
 *
 *****************************************************************************/
void class_unregister_all();

/******************************************************************************
 *
 * DESCRIPTION: 
 *	Create class.
 *
 * PARAMETERS:
 *	fields			Number of fields.
 *	locals			Number of locals.
 *	instructions		Instructions array.
 *	ins_len			Instructions array length.
 *
 * RETURN VALUE:
 *	The made class object.
 *
 *****************************************************************************/
struct class* class_make(size_t fields, size_t locals, 
	unsigned char* instructions, size_t ins_len);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Get number of fields in class.
 *
 * PARAMETERS:
 *	class			The class to interrogate.
 *
 * RETURN VALUE:
 *	Number of fields.
 *
 *****************************************************************************/
size_t class_field_count(struct class* class);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Get number of locals in class.
 *
 * PARAMETERS:
 *	class			The class to interrogate.
 *
 * RETURN VALUE:
 *	Number of locals.
 *
 *****************************************************************************/
size_t class_local_count(struct class* class);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Get number of instructions in class.
 *
 * PARAMETERS:
 *	class			The class to interrogate.
 *
 * RETURN VALUE:
 *	Number of instructions.
 *
 *****************************************************************************/
size_t class_ins_count(struct class* class);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Get instruction from class.
 *
 * PARAMETERS:
 *	class			The class to interrogate.
 *	position		The position of instruction (zero based).
 *
 * RETURN VALUE:
 *	The instruction, or 0 if index is out of range.
 *
 *****************************************************************************/
unsigned char class_get_ins(struct class* class, size_t position);


#endif
