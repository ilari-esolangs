#include "stdlibrary.h"
#include "object.h"
#include <stdio.h>
#include <stdlib.h>
#include "message.h"
#include "context.h"
#include "vat.h"

/*****************************************************************************/
static void capfuck_stdin_h(struct context* ctx, struct message* msg)
{
	struct object* dummy;
	struct object* o;
	struct object* target; 

	if(!(target = message_read_slot(msg, 0)))
		return;

	struct message* message = message_create(target, 9);

	int c = getchar();
	if(c >= 0) {
		dummy = object_read_field(message_target(msg), 0);
		message_write_slot(message, 0, dummy);
		for(unsigned i = 0; i < 8; i++) {
			o = ((c >> (7 - i)) & 1) ? dummy : NULL;
			message_write_slot(message, i + 1, o);
		}
	}
	context_queue_message(ctx, message);
}

/*****************************************************************************/
static void capfuck_stdX_h(FILE* file, struct context* ctx, 
	struct message* msg)
{
	unsigned char value = 0;
	for(unsigned i = 0; i < 8; i++)
		value = value * 2 + (message_read_slot(msg, i) ? 1 : 0);

	fprintf(file, "%c", value);
}


/*****************************************************************************/
static void capfuck_stdout_h(struct context* ctx, struct message* msg)
{
	capfuck_stdX_h(stdout, ctx, msg);
}

/*****************************************************************************/
static void capfuck_stderr_h(struct context* ctx, struct message* msg)
{
	capfuck_stdX_h(stderr, ctx, msg);
}

/*****************************************************************************/
struct stdlibrary* stdlibrary_init(struct vat* vat)
{
	struct object* capfuck_dummy;
	struct object* capfuck_stdin;
	struct object* capfuck_stdout;
	struct object* capfuck_stderr;

	struct stdlibrary* lib;

	lib = malloc(sizeof(struct stdlibrary));
	if(!lib) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	
	/* Create and add objects to vats. */
	capfuck_dummy = object_create_special(NULL, 0);
	capfuck_stdin = object_create_special(capfuck_stdin_h, 2);
	capfuck_stdout = object_create_special(capfuck_stdout_h, 0);
	capfuck_stderr = object_create_special(capfuck_stderr_h, 0);
	vat_add_object(vat, capfuck_stdin);
	vat_add_object(vat, capfuck_stdout);
	vat_add_object(vat, capfuck_stderr);
	vat_add_object(vat, capfuck_dummy);
	object_write_field(capfuck_stdin, 0, capfuck_dummy);

	lib->stdin = capfuck_stdin;
	lib->stdout = capfuck_stdout;
	lib->stderr = capfuck_stderr;
	return lib;
}
