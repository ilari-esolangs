#include "context.h"
#include "object.h"
#include "vat.h"
#include "message.h"
#include <stdio.h>
#include <stdlib.h>

/* Stack slot. */
struct stack
{
	/* Object reference held. */
	struct object* s_object;
	/* Next stack slot. */
	struct stack* s_next;
};

/* Context. */
struct context
{
	/* Vat used. */
	struct vat* c_vat;
	/* Message being handled. */
	struct message* c_message;
	/* Stack top pointer. */
	struct stack* c_stacktop;
	/* Instruction pointer. */
	size_t c_ip;
	/* Number register value. */
	size_t c_numreg;
	/* Alternative mode flag. */
	unsigned c_alt_flag;
	/* Context lock. */
	struct lock c_lock;
	/* Locals. */
	struct object* c_locals[];
};

/*****************************************************************************/
struct object* context_pop(struct context* ctx)
{
	if(!ctx->c_stacktop)
		return NULL;

	struct stack* tmp = ctx->c_stacktop;
	struct object* obj = tmp->s_object;
	ctx->c_stacktop = tmp->s_next;
	free(tmp);
	return obj;
}

/*****************************************************************************/
void context_push(struct context* ctx, struct object* obj)
{
	struct stack* newslot = malloc(sizeof(struct stack));
	if(!newslot) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	newslot->s_next = ctx->c_stacktop;
	newslot->s_object = obj;
	ctx->c_stacktop = newslot;
}

/*****************************************************************************/
unsigned context_prepare(struct context* ctx)
{
	/* Grab message queue lock and context lock. */
	vat_grab_queue_lock(ctx->c_vat);
	mutex_lock(&ctx->c_lock);

	ctx->c_message = vat_deque_message(ctx->c_vat);
	if(!ctx->c_message) 
		return 0;

	/* Mark object as being executed. */
	object_set_executing(message_target(ctx->c_message));

	/* Set fields. */
	ctx->c_stacktop = NULL;
	ctx->c_ip = 0;
	ctx->c_numreg = 0;
	ctx->c_alt_flag = 0;
	for(size_t i = 0; i < vat_maxlocals(ctx->c_vat); i++)
		ctx->c_locals[i] = NULL;

	mutex_unlock(&ctx->c_lock);
	vat_ungrab_queue_lock(ctx->c_vat);
	return 1;
}

/*****************************************************************************/
void context_unprepare(struct context* ctx)
{
	/* Grab message queue lock and context lock. */
	vat_grab_queue_lock(ctx->c_vat);
	mutex_lock(&ctx->c_lock);

	object_clear_executing(message_target(ctx->c_message));

	/* Kill the message. It is now handled. */
	free(ctx->c_message);

	/* Kill all the locals. */
	for(size_t i = 0; i < vat_maxlocals(ctx->c_vat); i++)
		ctx->c_locals[i] = NULL;
	/* Kill stack. */
	while(ctx->c_stacktop)
		context_pop(ctx);

	mutex_unlock(&ctx->c_lock);
	vat_ungrab_queue_lock(ctx->c_vat);
}

/*****************************************************************************/
size_t context_stacksize(struct context* ctx)
{
	size_t size = 0;
	struct stack* s = ctx->c_stacktop;
	while(s) {
		s = s->s_next;
		size++;
	}
	return size;
}


/*****************************************************************************/
void context_singlestep(struct context* ctx)
{
	struct message* msg = ctx->c_message;
	struct object* t = message_target(msg);
	struct class* tc = object_class(t);
	struct object* tmp1;
	struct object* tmp2;
	struct message* tmp4;

	/* Grab context lock. */
	mutex_lock(&ctx->c_lock);

	if(ctx->c_alt_flag) {
		switch(class_get_ins(tc, ctx->c_ip)) {
		case 'f':
		case 'F':
		case 'l':
		case 'L':
		case 'p':
		case 'P':
		case 's':
		case 'S':
		case 'N':
			/* Just clear the alternative mode. */
			ctx->c_alt_flag = 0;
			ctx->c_ip++;
			mutex_unlock(&ctx->c_lock);
			return;
		}
	}

	switch(class_get_ins(tc, ctx->c_ip)) {
	case '+':
		ctx->c_numreg++;
		break;
	case '-':
		ctx->c_numreg--;
		break;
	case 'E':
		tmp1 = context_pop(ctx);
		tmp2 = context_pop(ctx);
		if(tmp1 != tmp2)
			ctx->c_alt_flag = 1;
		break;
	case 'f':
		object_write_field(t, ctx->c_numreg, context_pop(ctx));
		break;
	case 'F':
		context_push(ctx, object_read_field(t, ctx->c_numreg));
		break;
	case 'l':
		tmp1 = context_pop(ctx);
		if(ctx->c_numreg < class_local_count(tc))
			ctx->c_locals[ctx->c_numreg] = tmp1;
		break;
	case 'L':
		if(ctx->c_numreg < class_local_count(tc))
			tmp1 = ctx->c_locals[ctx->c_numreg];
		else
			tmp1 = NULL;
		context_push(ctx, tmp1);
		break;
	case 'p':
		message_write_slot(msg, ctx->c_numreg, context_pop(ctx));
		break;
	case 'P':
		context_push(ctx, message_read_slot(msg, ctx->c_numreg));
		break;
	case 's':
		tmp1 = context_pop(ctx);
		if(!tmp1)
			break;

		/* Allocate it. */
		tmp4 = message_create(tmp1, context_stacksize(ctx));
		for(size_t i = 0; i < message_parameters(tmp4); i++)
			message_write_slot(tmp4, i, context_pop(ctx));
		vat_queue_message(ctx->c_vat, tmp4);
		break;
	case 'S':
		context_push(ctx, t);
		break;
	case 'N':
		tmp1 = object_create_ordinary(class_search(ctx->c_numreg));
		vat_add_object(ctx->c_vat, tmp1);
		context_push(ctx, tmp1);
		break;
	default:
		/* Can't happen! */
		;
	};
	ctx->c_ip++;
	mutex_unlock(&ctx->c_lock);
}

/*****************************************************************************/
struct context* context_create(struct vat* vat)
{
	struct context* ctx = malloc(sizeof(struct context) + 
		vat_maxlocals(vat) * sizeof(struct object*));
	if(!ctx) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}

	ctx->c_vat = vat;
	ctx->c_stacktop = NULL;
	ctx->c_ip = 0;
	ctx->c_numreg = 0;
	ctx->c_alt_flag = 0;
	for(size_t i = 0; i < vat_maxlocals(vat); i++)
		ctx->c_locals[i] = NULL;
	return ctx;
}

/*****************************************************************************/
void context_lock(struct context* ctx)
{
	mutex_lock(&ctx->c_lock);
}

/*****************************************************************************/
void context_unlock(struct context* ctx)
{
	mutex_unlock(&ctx->c_lock);
}

/*****************************************************************************/
void context_queue_message(struct context* context, struct message* message)
{
	vat_queue_message(context->c_vat, message);
}

/*****************************************************************************/
unsigned context_execute_msghandler(struct context* ctx)
{
	if(!context_prepare(ctx))
		return 0;

	struct message* m = ctx->c_message;
	struct object* t = message_target(m);
	struct class* tc = object_class(t);

	if(object_exec_special(t, ctx, m)) {
		context_unprepare(ctx);
		return 1;
	} else if(!tc) {
		/* Should not happen... */
		context_unprepare(ctx);
		return 1;
	}

	while(ctx->c_ip < class_ins_count(tc)) {
		context_singlestep(ctx);
	}

	context_unprepare(ctx);
	return 1;
}


