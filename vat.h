#ifndef _vat__h__included__
#define _vat__h__included__

#include <stdlib.h>
#include "lock.h"

struct message;
struct object;

/* Vat. */
struct vat;

/******************************************************************************
 *
 * DESCRIPTION:
 *	Grab queue lock.
 *
 * PARAMETERS:
 *	vat			The vat to manipulate.
 *
 *****************************************************************************/
void vat_grab_queue_lock();

/******************************************************************************
 *
 * DESCRIPTION:
 *	Ungrab queue lock.
 *
 * PARAMETERS:
 *	vat			The vat to manipulate.
 *
 *****************************************************************************/
void vat_ungrab_queue_lock();

/******************************************************************************
 *
 * DESCRIPTION:
 *	Queue message into vat message queue.
 *
 * PARAMETERS: 
 *	vat			The vat to use.
 *	message			The message to queue.
 *
 *****************************************************************************/
void vat_queue_message(struct vat* vat, struct message* message);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Perform full mark and sweep GC on vat.
 *
 * PARAMETERS: 
 *	vat			The vat to use.
 *
 * RETURN VALUE:
 *	1 if objects still remain, 0 if vat is now empty.
 *
 *****************************************************************************/
unsigned vat_perform_gc(struct vat* vat);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Get maximum number of locals.
 *
 * PARAMETERS: 
 *	vat			The vat to use.
 *
 * RETURN VALUE:
 *	Maximum locals count.
 *
 *****************************************************************************/
size_t vat_maxlocals(struct vat* vat);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Create vat.
 *
 * PARAMETERS: 
 *	maxlocals		Maximum locals count.
 *
 * RETURN VALUE:
 *	The vat.
 *
 *****************************************************************************/
struct vat* vat_create(size_t maxlocals);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Destroy vat, and all objects in it.
 *
 * PARAMETERS: 
 *	vat			The vat to destroy.
 *
 *****************************************************************************/
void vat_destroy(struct vat* vat);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Add object to vat.
 *
 * PARAMETERS: 
 *	vat			The vat to manipulate.
 *	object			The object to add.
 *
 *****************************************************************************/
void vat_add_object(struct vat* vat, struct object* object);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Deque message from vat message queue.
 *
 * PARAMETERS: 
 *	vat			The vat to manipulate.
 *
 * RETURN VALUE:
 *	Message dequed.
 *
 *****************************************************************************/
struct message* vat_deque_message(struct vat* vat);

#endif
