#ifndef _message__h__included__
#define _message__h__included__

struct object;

/* Message. */
struct message;

/* Message queue. */
struct message_queue;

/******************************************************************************
 *
 * DESCRIPTION:
 *	Create message queue.
 *
 * RETURN VALUE:
 *	Message queue.
 *
 *****************************************************************************/
struct message_queue* message_queue_create();

/******************************************************************************
 *
 * DESCRIPTION:
 *	Destroy message queue and all messages in it.
 *
 * PARAMETERS:
 *	queue			The message queue to destroy.
 *
 *****************************************************************************/
void message_queue_destroy(struct message_queue* queue);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Add message to queue.
 *
 * PARAMETERS:
 *	queue			The message queue to add message to.
 *	msg			The message to add.
 *
 *****************************************************************************/
void message_queue_queue(struct message_queue* queue, struct message* msg);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Deque message from queue. Only messages destined to objects with
 *	executing flag clear are considered.
 *
 * PARAMETERS:
 *	queue			The message queue to deque from
 *
 * RETURN VALUE:
 *	The dequed message, or NULL if no suitable message exists.
 *
 *****************************************************************************/
struct message* message_queue_deque(struct message_queue* queue);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Call callback for all messages in queue.
 *
 * PARAMETERS:
 *	queue			The message queue to handle.
 *	callback		The callback to call.
 *
 *****************************************************************************/
void message_queue_forall(struct message_queue* queue,
	void (*callback)(struct message* msg));

/******************************************************************************
 *
 * DESCRIPTION:
 *	Get message target.
 *
 * PARAMETERS:
 *	msg			The message.
 *
 * RETURN VALUE:
 *	Message target object.
 *
 *****************************************************************************/
struct object* message_target(struct message* msg);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Get number of parameters in message.
 *
 * PARAMETERS:
 *	msg			The message.
 *
 * RETURN VALUE:
 *	Number of parameters.
 *
 *****************************************************************************/
size_t message_parameters(struct message* msg);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Read slot value. Reading undefined slot gives NULL.
 *
 * PARAMETERS:
 *	msg			The message.
 *	slot			Slot nubmer.
 *
 * RETURN VALUE:
 *	Reference in slot.
 *
 *****************************************************************************/
struct object* message_read_slot(struct message* msg, size_t slot);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Write slot value. Writing undefined slot is no-operation.
 *
 * PARAMETERS:
 *	msg			The message.
 *	slot			Slot nubmer.
 *	obj			Object to write.
 *	
 *****************************************************************************/
void message_write_slot(struct message* msg, size_t slot, struct object* obj);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Create new message.
 *
 * PARAMETERS:
 *	target			Target of message.
 *	parameters		Number of parameters.
 *
 * RETURN VALUE:
 *	Constructed message. Free with free.
 *	
 *****************************************************************************/
struct message* message_create(struct object* target, size_t parameters);

#endif
