#ifndef _object__h__included__
#define _object__h__included__

#include "class.h"

struct context;
struct message;

/* Object marked for garbage collection. */
#define OF_MARKED_FOR_GC 1
/* Already execuing message handler. */
#define OF_EXECUTING 2

/* Object. */
struct object;

/* Object list. */
struct objectlist;

/******************************************************************************
 *
 * DESCRIPTION:
 *	Read object field.
 *
 * PARAMETERS:
 *	object			The object to read.
 *	slot			Field slot number.
 *
 * RETURN VALUE:
 *	Read value, or NULL if slot is invalid.
 *
 *****************************************************************************/
struct object* object_read_field(struct object* object, size_t slot);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Write object field. Writes to nonexistent slots are discarded.
 *
 * PARAMETERS:
 *	object			The object to write.
 *	slot			Field slot number.
 *	towrite			Refrence to write.
 *
 *****************************************************************************/
void object_write_field(struct object* object, size_t slot, 
	struct object* towrite);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Get class of object.
 *
 * PARAMETERS:
 *	object			The object to interrogate.
 *
 * RETURN VALUE:
 *	Class of object.
 *
 *****************************************************************************/
struct class* object_class(struct object* object);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Check object EXECUTING flag.
 *
 * PARAMETERS:
 *	object			The object to interrogate.
 *
 * RETURN VALUE:
 *	1 if object is executing, 0 otherwise.
 *
 *****************************************************************************/
unsigned object_is_executing(struct object* object);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Set object EXECUTING flag.
 *
 * PARAMETERS:
 *	object			The object to manipulate.
 *
 *****************************************************************************/
void object_set_executing(struct object* object);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Clear object EXECUTING flag.
 *
 * PARAMETERS:
 *	object			The object to manipulate.
 *
 *****************************************************************************/
void object_clear_executing(struct object* object);


/******************************************************************************
 *
 * DESCRIPTION:
 *	Check object MARKED_FOR_GC flag.
 *
 * PARAMETERS:
 *	object			The object to interrogate.
 *
 * RETURN VALUE:
 *	1 if object is marked for GC, 0 otherwise.
 *
 *****************************************************************************/
unsigned object_is_gcmark(struct object* object);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Set object MARKED_FOR_GC flag.
 *
 * PARAMETERS:
 *	object			The object to manipulate.
 *
 *****************************************************************************/
void object_set_gcmark(struct object* object);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Clear object MARKED_FOR_GC flag.
 *
 * PARAMETERS:
 *	object			The object to manipulate.
 *
 *****************************************************************************/
void object_clear_gcmark(struct object* object);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Create object of specified class.
 *
 * PARAMETERS:
 *	class			The class of new object.
 *
 * RETURN VALUE:
 *	The newly created object.
 *
 *****************************************************************************/
struct object* object_create_ordinary(struct class* class);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Create special object.
 *
 * PARAMETERS:
 *	handler			Handler object.
 *	fields			Fields for local object. Note that if this
 *				is nonzero, new class is created and
 *				registered.
 *
 * RETURN VALUE:
 *	The newly created object.
 *
 *****************************************************************************/
struct object* object_create_special(void (*handler)(struct context* ctx, 
	struct message* msg), size_t fields);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Create object list.
 *
 * RETURN VALUE:
 *	The newly created object list.
 *
 *****************************************************************************/
struct objectlist* objectlist_create();

/******************************************************************************
 *
 * DESCRIPTION:
 *	Destroy object list and all objects in it.
 *
 * PARAMETERS:
 *	objectlist		Object list to destroy.
 *
 *****************************************************************************/
void objectlist_destroy(struct objectlist* objectlist);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Add object to object list.
 *
 * PARAMETERS:
 *	objectlist		Object list to add object to.
 *	object			Object to add.
 *
 *****************************************************************************/
void objectlist_add(struct objectlist* objectlist, struct object* object);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Kill all marked objects from list.
 *
 * PARAMETERS:
 *	objectlist		Object list to manipulate.
 *
 * RETURN VALUE:
 *	1 if more remain, 0 if all are gone.
 *
 *****************************************************************************/
unsigned objectlist_kill_marked(struct objectlist* objectlist);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Set object MARKED_FOR_GC flag for all objects in list.
 *
 * PARAMETERS:
 *	objectlist		The object list to manipulate.
 *
 *****************************************************************************/
void objectlist_set_gcmark_all(struct objectlist* objectlist);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Execute special action, if any.
 *
 * PARAMETERS:
 *	object			The object.
 *	ctx			Context to pass in.
 *	msg			Message to pass in.
 *
 * RETURN VALUE:
 *	1 if special action was executed, 0 if there is none.
 *
 *****************************************************************************/
unsigned object_exec_special(struct object* object, struct context* ctx,
	struct message* msg);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Clear MARKED_FOR_GC for this object and all reachable objects.
 *
 * PARAMTERS:
 *	object			GC anchor to add.
 *
 *****************************************************************************/
void object_add_anchor(struct object* object);


#endif
