#include "vat.h"
#include "lock.h"
#include "object.h"
#include "message.h"
#include <stdio.h>

/* Vat. */
struct vat
{
	/* Object list. */
	struct objectlist* v_objectlist;
	/* Object list head. */
	struct object* v_object_list;
	/* Message queue head. */
	struct message_queue* v_msg_queue;
	/* Greatest locals count for any class. */
	size_t v_max_locals;
	/* Message queue lock. */
	struct lock v_msgq_lock;
};

/*****************************************************************************/
void vat_grab_queue_lock(struct vat* vat)
{
	mutex_lock(&vat->v_msgq_lock);
}

/*****************************************************************************/
void vat_ungrab_queue_lock(struct vat* vat)
{
	mutex_unlock(&vat->v_msgq_lock);
}

/*****************************************************************************/
void vat_queue_message(struct vat* vat, struct message* message)
{
	message_queue_queue(vat->v_msg_queue, message);
}

/*****************************************************************************/
static void mark_message_live(struct message* msg)
{
	object_add_anchor(message_target(msg));
	for(size_t i = 0; i < message_parameters(msg); i++) {
		object_add_anchor(message_read_slot(msg, i));
	}
}

/*****************************************************************************/
unsigned vat_perform_gc(struct vat* vat)
{
	objectlist_set_gcmark_all(vat->v_objectlist);
	message_queue_forall(vat->v_msg_queue, mark_message_live);
	return objectlist_kill_marked(vat->v_objectlist);
}

/*****************************************************************************/
size_t vat_maxlocals(struct vat* vat)
{
	return vat->v_max_locals;
}

/*****************************************************************************/
struct vat* vat_create(size_t maxlocals)
{
	struct vat* vat;
	vat = malloc(sizeof(struct vat));
	if(!vat) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	vat->v_objectlist = objectlist_create();
	vat->v_msg_queue = message_queue_create();
	vat->v_max_locals = maxlocals;
	return vat;
}

/*****************************************************************************/
void vat_destroy(struct vat* vat)
{
	message_queue_destroy(vat->v_msg_queue);
	objectlist_destroy(vat->v_objectlist);
	free(vat);
}

/*****************************************************************************/
void vat_add_object(struct vat* vat, struct object* object)
{
	objectlist_add(vat->v_objectlist, object);
}

/*****************************************************************************/
struct message* vat_deque_message(struct vat* vat)
{
	return message_queue_deque(vat->v_msg_queue);
}
