#include "object.h"
#include <stdio.h>

/* Object. */
struct object
{
	/* Flags. */
	unsigned o_flags;
	/* Class of object. */
	struct class* o_class;
	/* Special receiver. */
	void (*o_special)(struct context* ctx, struct message* msg);
	/* Fields. */
	struct object* o_fields[];
};


/* Object list node. */
struct objectlist_node
{
	/* Object pointed. */
	struct object* oln_object;
 	/* Next node. */
	struct objectlist_node* oln_next;
};


/* Object list. */
struct objectlist
{
	/* Root of list. */
	struct objectlist_node* ol_first;
};


/*****************************************************************************/
struct object* object_read_field(struct object* object, size_t slot)
{
	if(slot < class_field_count(object->o_class))
		return object->o_fields[slot];
	else
		return NULL;
}

/*****************************************************************************/
void object_write_field(struct object* object, size_t slot, 
	struct object* towrite)
{
	if(slot < class_field_count(object->o_class))
		object->o_fields[slot] = towrite;
}

/*****************************************************************************/
unsigned object_is_executing(struct object* object)
{
	return ((object->o_flags & OF_EXECUTING) ? 1 : 0);
}

/*****************************************************************************/
void object_set_executing(struct object* object)
{
	object->o_flags |= OF_EXECUTING;
}

/*****************************************************************************/
void object_clear_executing(struct object* object)
{
	object->o_flags &= ~OF_EXECUTING;
}

/*****************************************************************************/
void object_set_gcmark(struct object* object)
{
	object->o_flags |= OF_MARKED_FOR_GC;
}

/*****************************************************************************/
void object_clear_gcmark(struct object* object)
{
	object->o_flags &= ~OF_MARKED_FOR_GC;
}

/*****************************************************************************/
unsigned object_is_gcmark(struct object* object)
{
	return ((object->o_flags & OF_MARKED_FOR_GC) ? 1 : 0);
}



/*****************************************************************************/
struct objectlist* objectlist_create()
{
	struct objectlist* objectlist;
	objectlist = malloc(sizeof(struct objectlist*));
	if(!objectlist) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	objectlist->ol_first = NULL;
	return objectlist;
}

/*****************************************************************************/
void objectlist_destroy(struct objectlist* objectlist)
{
	while(objectlist->ol_first) {
		struct objectlist_node* tmp = objectlist->ol_first;
		objectlist->ol_first = tmp->oln_next;
		free(tmp->oln_object);
		free(tmp);
	}
	free(objectlist);
}

/*****************************************************************************/
void objectlist_add(struct objectlist* objectlist, struct object* object)
{
	struct objectlist_node* node;
	node = malloc(sizeof(struct objectlist_node));
	if(!node) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	node->oln_object = object;
	node->oln_next = objectlist->ol_first;
	objectlist->ol_first = node;
}

/*****************************************************************************/
unsigned objectlist_kill_marked(struct objectlist* objectlist)
{
	struct objectlist_node* tmp = objectlist->ol_first;
	struct objectlist_node* tmp2;
	while(tmp && tmp->oln_object->o_flags & OF_MARKED_FOR_GC)
	{
		objectlist->ol_first = tmp->oln_next;
		free(tmp->oln_object);
		free(tmp);
		tmp = objectlist->ol_first;
	}
	if(!objectlist->ol_first)
		return 0;

	tmp = objectlist->ol_first;
	while(tmp && tmp->oln_next) {
		if(tmp->oln_next->oln_object->o_flags & OF_MARKED_FOR_GC) {
			tmp2 = tmp->oln_next;
			tmp->oln_next = tmp2->oln_next;
			free(tmp2->oln_object);
			free(tmp2);
		}
		tmp = tmp->oln_next;
	}
	return 1;
}

/*****************************************************************************/
void objectlist_set_gcmark_all(struct objectlist* objectlist)
{
	struct objectlist_node* tmp = objectlist->ol_first;
	while(tmp) {
		object_set_gcmark(tmp->oln_object);
		tmp = tmp->oln_next;
	}
}

/*****************************************************************************/
struct object* object_create_ordinary(struct class* class)
{
	size_t fields;
	struct object* object;
	
	fields = class ? class_field_count(class) : 0;
	object = malloc(sizeof(struct object) + fields *
		sizeof(struct object*));
	if(!object) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	object->o_flags = 0;
	object->o_class = class;
	object->o_special = NULL;
	for(size_t i = 0; i < fields; i++)
		object->o_fields[i] = NULL;			
	return object;
}

/*****************************************************************************/
struct object* object_create_special(void (*handler)(struct context* ctx, 
	struct message* msg), size_t fields)
{
	struct object* object;
	struct class* class;
	
	object = malloc(sizeof(struct object) + fields *
		sizeof(struct object*));
	if(!object) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	object->o_flags = 0;
	object->o_class = NULL;
	object->o_special = handler;
	for(size_t i = 0; i < fields; i++)
		object->o_fields[i] = NULL;			

	if(fields > 0) {
		class = class_make(fields, 0, NULL, 0);
		class_register(class);
		object->o_class = class;
	}

	return object;
}

/*****************************************************************************/
struct class* object_class(struct object* object)
{
	return object->o_class;
}

/*****************************************************************************/
unsigned object_exec_special(struct object* object, struct context* ctx,
	struct message* msg)
{
	if(object->o_special) {
		object->o_special(ctx, msg);
		return 1;
	} else
		return 0;
}

/*****************************************************************************/
void object_add_anchor(struct object* object)
{
	size_t fields;

	/* Don't recurse twice to same object. */
 	if(!object || !object_is_gcmark(object))
		return;
	object_clear_gcmark(object);
	struct class* class = object_class(object);

	fields = class ? class_field_count(class) : 0;
	for(size_t i = 0; i < fields; i++) {
		object_add_anchor(object_read_field(object, i));
	}
}

