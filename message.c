#include "object.h"
#include "message.h"
#include <stdio.h>
#include <stdlib.h>

/* Message. */
struct message
{
	/* Target object. */
	struct object* m_target;
	/* Number of parameters in message. */
	size_t m_parameter_count;
	/* Parameters contained. */
	struct object* m_parameters[];
};

/* Message queue node. */
struct message_queue_node
{
	/* Message. */
	struct message* mqn_message;
	/* Next in list. */
	struct message_queue_node* mqn_next;
};

/* Message queue. */
struct message_queue
{
	/* First message in queue. */
	struct message_queue_node* mq_first;
	/* Last message in queue. */
	struct message_queue_node* mq_last;
};

/*****************************************************************************/
struct message_queue* message_queue_create()
{
	struct message_queue* queue;

	queue = malloc(sizeof(struct message_queue));
	if(!queue) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	queue->mq_first = NULL;
	queue->mq_last = NULL;
	return queue;
}

/*****************************************************************************/
void message_queue_destroy(struct message_queue* queue)
{
	struct message_queue_node* tmp;
	while(queue->mq_first) {
		tmp = queue->mq_first;
		queue->mq_first = queue->mq_first->mqn_next;
		free(tmp->mqn_message);
		free(tmp);
	}
	free(queue);
}

/*****************************************************************************/
void message_queue_queue(struct message_queue* queue, struct message* msg)
{
	struct message_queue_node* node;
	
	node = malloc(sizeof(struct message_queue_node));
	if(!node) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}

	node->mqn_message = msg;
	node->mqn_next = NULL;
	if(queue->mq_last)
		queue->mq_last = queue->mq_last->mqn_next = node;
	else
		queue->mq_last = queue->mq_first = node;
}

/*****************************************************************************/
struct message* message_queue_deque(struct message_queue* queue)
{
	if(!queue->mq_first)
		return NULL;
	
	/* Is the first message returnable? */
	if(!object_is_executing(queue->mq_first->mqn_message->m_target)) {
		struct message_queue_node* node = queue->mq_first;
		struct message* msg = node->mqn_message;
		queue->mq_first = node->mqn_next;
		free(node);
		if(!queue->mq_first)
			queue->mq_last = NULL;
		return msg;
	}

	/* Scan the rest of list. */
	struct message_queue_node* itr = queue->mq_first;
	while(itr->mqn_next)
		if(object_is_executing(itr->mqn_next->mqn_message->m_target))
			itr = itr->mqn_next;
	if(!itr->mqn_next)
		return NULL;

	
	struct message_queue_node* node = itr->mqn_next;
	struct message* msg = node->mqn_message;
	itr->mqn_next = node->mqn_next;
	free(node);
	if(queue->mq_last == node)
		queue->mq_last = itr;
	return msg;

}

/*****************************************************************************/
void message_queue_forall(struct message_queue* queue,
	void (*callback)(struct message* msg))
{
	struct message_queue_node* itr = queue->mq_first;
	while(itr) {
		callback(itr->mqn_message);
		itr = itr->mqn_next;
	}
}

/*****************************************************************************/
struct object* message_target(struct message* msg) 
{
	return msg->m_target;
}

/*****************************************************************************/
size_t message_parameters(struct message* msg)
{
	return msg->m_parameter_count;
}

/*****************************************************************************/
struct object* message_read_slot(struct message* msg, size_t slot)
{
	if(slot < msg->m_parameter_count)
		return msg->m_parameters[slot];
	else
		return NULL;
}

/*****************************************************************************/
void message_write_slot(struct message* msg, size_t slot, struct object* obj)
{
	if(slot < msg->m_parameter_count)
		msg->m_parameters[slot] = obj;
}

/*****************************************************************************/
struct message* message_create(struct object* target, size_t parameters)
{
	struct message* msg;
	msg = malloc(sizeof(struct message) + parameters *
		sizeof(struct object*));
	if(!msg) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	msg->m_target = target;
	msg->m_parameter_count = parameters;
	for(size_t i = 0; i < parameters; i++)
		msg->m_parameters[i] = NULL;
	
	return msg;
}

