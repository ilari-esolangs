#include <stdio.h>
#include <stdlib.h>
#include "sane-ctype.h"
#include "class.h"
#include "object.h"
#include "message.h"
#include "vat.h"
#include "context.h"
#include "stdlibrary.h"
#include "parser.h"

/*****************************************************************************/
int main(int argc, char** argv)
{
	int x;
	FILE* input;
	struct object* object;
	struct message* message;
	struct vat* vat;
	struct context* context;
	struct stdlibrary* lib;
	struct class* class;
	struct parser* parser;
	size_t maxlocals;

	if(argc == 1) {
		fprintf(stderr, "syntax: %s program.\n", argv[0]);
		return 2;
	}

	input = fopen(argv[1], "rb");
	if(input == NULL) {
		fprintf(stderr, "Can't open input file \"%s\".\n",
			argv[1]);
		return 1;
	}

	/* Parse the input. */
	parser = parser_create();
	while((x = fgetc(input)) >= 0) {
		parser_input(parser, (char)x);
	}
	parser_eof(parser);
	maxlocals = parser_maxlocals(parser);
	parser_destroy(parser);
	fclose(input);

	/* Find the initial class. */
	class = class_search(0);
	if(!class) {
		fprintf(stderr, "No classes defined.\n");
		exit(1);
	}

	/* Create initial object complement. */
	object = object_create_ordinary(class);

	/* Initialize vat. */
	vat = vat_create(maxlocals);
	vat_add_object(vat, object);
	lib = stdlibrary_init(vat);

	/* Initialize message. */
	message = message_create(object, 3);
	message_write_slot(message, 0, lib->stdin);
	message_write_slot(message, 1, lib->stdout);
	message_write_slot(message, 2, lib->stderr);
	free(lib);
	vat_queue_message(vat, message);

	/* Initialize context. */
	context = context_create(vat);

	while(1) {
		context_execute_msghandler(context);
		context_lock(context);
		if(!vat_perform_gc(vat))
			break;
		context_unlock(context);
	}

	context_unlock(context);
	vat_destroy(vat);
	free(context);

	class_unregister_all();
	fflush(stdout);
	return 0;
}
