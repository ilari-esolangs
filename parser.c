#include "parser.h"
#include <stdio.h>
#include "sane-ctype.h"
#include "class.h"



/* on line, no non-whitespace yet. */
#define PS_INITIAL 0
/* Last was non-whitespace. */
#define PS_LAST_NOT_WS 1
/* Last was whitespace, non-ws seen. */
#define PS_WHITESPACE 2
/* Last was newline. */
#define PS_NEWLINE 3
/* Last was Carriage return. */
#define PS_LAST_CR 4
/* Start of line. */
#define PS2_INITIAL 0
/* On non-comment line. */
#define PS2_NOT_COMMENT 1
/* On comment line. */
#define PS2_COMMENT 2

/* Parse context. */
struct parser
{
	/* Line number. */
	unsigned p_line;
	/* Main parser status. */
	unsigned p_status;
	/* Secondary parser status. */
	unsigned p_status2;
	/* Backslash flag. */
	unsigned p_backslash;
	/* Number of class to parse. */
	size_t p_classnum;
	/* Number of fields. */
	size_t p_fields;
	/* Number of locals. */
	size_t p_locals;
	/* Max number of locals. */
	size_t p_maxlocals;
	/* Allocated size of instructions. */
	size_t p_ins_allocated;
	/* Real size of instructions. */
	size_t p_ins_size;
	/* Instructions. */
	unsigned char* p_ins;
	/* Field number. */
	unsigned p_field;
	/* Have value for field. 0 = no, 1 = zero, 2 = nonzero. */
	unsigned p_have_field_value;
};

#define CARRIAGE_RETURN 13
#define SPACE 32
#define LINEFEED 10
#define BACKSLASH 92
#define HASH 35
#define ZERO 48
#define NINE 57


/*****************************************************************************/
struct parser* parser_create()
{
	struct parser* parser;
	parser = malloc(sizeof(struct parser));
	if(!parser) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}

	parser->p_line = 1;
	parser->p_status = PS_INITIAL;
	parser->p_status2 = PS2_INITIAL;
	parser->p_backslash = 0;
	parser->p_classnum = 0;
	parser->p_fields = 0;
	parser->p_locals = 0;
	parser->p_maxlocals = 0;
	parser->p_ins_allocated = 0;
	parser->p_ins_size = 0;
	parser->p_ins = NULL;
	parser->p_field = 0;
	parser->p_have_field_value = 0;

	return parser;	
}

/*****************************************************************************/
void parser_destroy(struct parser* parser)
{
	free(parser->p_ins);
	free(parser);
}

/*****************************************************************************/
static void parser_input3(struct parser* parser, char input)
{
	if(input == SPACE) {
		if(parser->p_field == 2) {
			fprintf(stderr, "Line %zu: Illegal character <%02X> "
				"in class body.\n", parser->p_line, input);
			exit(1);
		}
		parser->p_field++;
		parser->p_have_field_value = 0;
	} else if(input == LINEFEED) {
		if(parser->p_field < 1) {
			fprintf(stderr, "Line %zu: Truncated class "
				"description.\n", parser->p_line);
			exit(1);
		}
		/* Parsed class. */
		if(parser->p_locals > parser->p_maxlocals)
			parser->p_maxlocals = parser->p_locals;

		struct class* class;
		class = class_make(parser->p_fields, parser->p_locals,
			parser->p_ins, parser->p_ins_size);
		class_register(class);

		parser->p_ins = NULL;
		parser->p_ins_allocated = 0;
		parser->p_ins_size = 0;
		parser->p_have_field_value = 0;
		parser->p_field = 0;
		parser->p_fields = 0;
		parser->p_locals = 0;
	} else if(parser->p_field < 2) {
		if(input < ZERO && input > NINE) {
			fprintf(stderr, "Line %zu: Illegal character <%02X> "
				"in numeric value.\n", parser->p_line,
				input);
			exit(1);
		}
		if(input == ZERO) {
			if(parser->p_have_field_value == 1) {
				fprintf(stderr, "Line %zu: Invalid numeric "
					"value.\n", parser->p_line);
				exit(1);

			}
		}

		if(parser->p_field == 0)
			parser->p_fields = 10 * parser->p_fields + 
				(input - ZERO);
		else
			parser->p_locals = 10 * parser->p_locals + 
				(input - ZERO);

	} else {
		switch(input) {
		case '+':
		case '-':
		case 'E':
		case 'f':
		case 'F':
		case 'l':
		case 'L':
		case 'p':
		case 'P':
		case 's':
		case 'S':
		case 'N':
			break;
		default:
			fprintf(stderr, "Line %zu: Invalid instruction "
				"<%02X>.\n", parser->p_line, input);
			exit(1);
		}
		if(parser->p_ins_size == parser->p_ins_allocated) {
			if(parser->p_ins_allocated == 0)
				parser->p_ins_allocated = 2;
			parser->p_ins_allocated = parser->p_ins_allocated * 
				3 / 2;
			parser->p_ins = realloc(parser->p_ins, 
				parser->p_ins_allocated);
			if(!parser->p_ins) {
				fprintf(stderr, "Out of memory.\n");
				exit(1);
			}
		}
		parser->p_ins[parser->p_ins_size++] = input;
	}
}

/*****************************************************************************/
static void parser_input2(struct parser* parser, char input)
{
	/* Here, we got to handle comments and backslashes. */
	if(parser->p_backslash && (input == LINEFEED || input == SPACE))
		return;
	parser->p_backslash = (input == BACKSLASH);
	if(input == BACKSLASH)
		return;

	switch(parser->p_status2) {
	case PS2_INITIAL:
		if(input == HASH) 
			parser->p_status2 = PS2_COMMENT;
		else {
			parser->p_status2 = PS2_NOT_COMMENT;
			parser_input3(parser, input);
		}
		break;
	case PS2_NOT_COMMENT:
		parser_input3(parser, input);
		/* Fallthrough. */
	case PS2_COMMENT:
		if(input == LINEFEED)
			parser->p_status2 = PS2_INITIAL;
		break;
	}
}

/*****************************************************************************/
void parser_input(struct parser* parser, char input)
{
	/* We need to clean up input so that whitespaces are normalized. */
	unsigned is_space = sane_isspace((unsigned char)input);
	unsigned is_newline = sane_isnewline((unsigned char)input);

	switch(parser->p_status) {
	case PS_INITIAL:
		if(is_newline) {
			parser->p_status = ((input == CARRIAGE_RETURN) ?
				PS_LAST_CR : PS_NEWLINE);
			parser->p_line++;
		} else if(is_space) {
			/* Keep state. */
		} else {
			parser->p_status = PS_LAST_NOT_WS;
			parser_input2(parser, input);
		} 
		break;
	case PS_LAST_NOT_WS:
		if(is_newline) {
			parser_input2(parser, LINEFEED);
			parser->p_status = ((input == CARRIAGE_RETURN) ?
				PS_LAST_CR : PS_NEWLINE);
			parser->p_line++;
		} else if(is_space) {
			parser->p_status = PS_WHITESPACE;
		} else {
			parser_input2(parser, input);
		}
		break;
	case PS_WHITESPACE:
		if(is_newline) {
			parser_input2(parser, LINEFEED);
			parser->p_status = ((input == CARRIAGE_RETURN) ?
				PS_LAST_CR : PS_NEWLINE);
			parser->p_line++;
		} else if(is_space) {
			/* Keep state. */
		} else {
			parser->p_status = PS_LAST_NOT_WS;
			parser_input2(parser, SPACE);
			parser_input2(parser, input);
		}
		break;
	case PS_NEWLINE:
		if(is_newline) {
			parser->p_status = ((input == CARRIAGE_RETURN) ?
				PS_LAST_CR : PS_NEWLINE);
			parser->p_line++;
		} else if(is_space) {
			parser->p_status = PS_INITIAL;
		} else {
			parser->p_status = PS_LAST_NOT_WS;
			parser_input2(parser, input);
		}
		break;
	case PS_LAST_CR:
		if(is_newline) {
			parser->p_status = ((input == CARRIAGE_RETURN) ?
				PS_LAST_CR : PS_NEWLINE);
			if(input != LINEFEED)
				parser->p_line++;
		} else if(is_space) {
			parser->p_status = PS_INITIAL;
		} else {
			parser->p_status = PS_LAST_NOT_WS;
			parser_input2(parser, input);
		}
		break;
	}
}

/*****************************************************************************/
void parser_eof(struct parser* parser)
{
	if(parser->p_status == PS_LAST_NOT_WS || 
			parser->p_status == PS_WHITESPACE)
		parser_input2(parser, LINEFEED);
}

/*****************************************************************************/
size_t parser_maxlocals(struct parser* parser)
{
	return parser->p_maxlocals;
}


