#ifndef _parser__h__included__
#define _parser__h__included__

#include <stdlib.h>

/* Parser. */
struct parser;

/******************************************************************************
 *
 * DESCRIPTION:
 *	Create parser.
 *
 * RETURN VALUE:
 *	parser created.
 *
 *****************************************************************************/
struct parser* parser_create();

/******************************************************************************
 *
 * DESCRIPTION:
 *	Destroy parser.
 *
 * PARAMETERS:
 *	parser			Parser to destroy.
 *
 *****************************************************************************/
void parser_destroy(struct parser* parser);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Push character to parser.
 *
 * PARAMETERS:
 *	parser			Parser to use.
 *	input			Input character.
 *
 *****************************************************************************/
void parser_input(struct parser* parser, char input);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Push End-Of-File character to parser.
 *
 * PARAMETERS:
 *	parser			Parser to use.
 *
 *****************************************************************************/
void parser_eof(struct parser* parser);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Get maximum locals count.
 *
 * PARAMETERS:
 *	parser			Parser to use.
 *
 * RETURN VALUE:
 *	Maximum of locals count over all classes.
 *
 *****************************************************************************/
size_t parser_maxlocals(struct parser* parser);


#endif
