#ifndef _lock__h__included__
#define _lock__h__included__

/* Lock (mutex). */
struct lock
{
};

/******************************************************************************
 *
 * DESCRIPTION:
 *	Lock mutex.
 *
 * PARAMETERS: 
 *	lock			Mutex to lock.
 *
 *****************************************************************************/
void mutex_lock(struct lock* lock);

/******************************************************************************
 *
 * DESCRIPTION:
 *	Unlock mutex.
 *
 * PARAMETERS: 
 *	lock			Mutex to unlock.
 *
 *****************************************************************************/
void mutex_unlock(struct lock* lock);

#endif

